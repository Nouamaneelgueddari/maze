package de.fhac.mazenet.client;


import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.bind.UnmarshalException;

import de.fhac.mazenet.client.game.BoardHelper;
import de.fhac.mazenet.client.game.PinPosition;
import de.fhac.mazenet.client.game.PinShift;
import de.fhac.mazenet.client.game.ReachShift;
import de.fhac.mazenet.client.game.ShiftPositionData;
import de.fhac.mazenet.client.game.Shifts;
import de.fhac.mazenet.server.game.*;
import de.fhac.mazenet.server.generated.*;
import de.fhac.mazenet.server.networking.*;

public class Client {
	private static  Socket clientsocket;
	private static  XmlOutputStream outToServer;
	private static  XmlInputStream inFromServer;
	private static  int playerId;
	private static  PinPosition pinpos = new PinPosition(0,0);
	private static  Treasure t;
	private static boolean finish = false;
	private static Shifts  allshifts  ;
	private static PositionData verbotenpos = new PositionData();
	/*public Client() throws UnknownHostException, IOException{
		clientsocket = new Socket("127.0.0.1", 5123);// verbindet der Client zu dem Server
		inFromServer = new XmlInputStream(clientsocket.getInputStream());
		outToServer = new XmlOutputStream(clientsocket.getOutputStream());
	}*/
	
	public static void main(String[]args) {
		try {
			File fr  = new File("C:\\Users\\HP\\workspace\\maze-client1\\src\\test\\java\\maze_client1\\ConfigDatei.txt"); 
			var sc = new Scanner(fr);
			List<String> infos = new ArrayList<String>();
			while(sc.hasNext()) {
				infos.add(sc.nextLine());
			}
			var sc1 = new Scanner(System.in);
			clientsocket = new Socket(infos.get(0), Integer.parseInt(infos.get(1)));// verbindet der Client zu dem Server
			inFromServer = new XmlInputStream(clientsocket.getInputStream());
			outToServer = new XmlOutputStream(clientsocket.getOutputStream());
			System.out.println("Name eingeben:");
			MazeCom mazecom = MazeComMessageFactory.createLoginMessage(sc1.nextLine());
			outToServer.write(mazecom);
			MazeCom maze;
			while (finish == false) {
				maze = inFromServer.readMazeCom();
				allshifts = new Shifts();
				mazeComMessageverwaltung(maze);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// TODO Auto-generated catch block
		} catch (UnmarshalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static  void mazeComMessageverwaltung(MazeCom maze) {
		System.out.println(maze.getMessagetype());
		switch (maze.getMessagetype()) {
		case LOGINREPLY:
			playerId = maze.getLoginReplyMessage().getNewID();
			break;
		case AWAITMOVE:
			System.out.println(maze.getMessagetype());
			Board clientBoard = new Board(maze.getAwaitMoveMessage().getBoard());
			MazeCom maze1 = new MazeCom();
			t=maze.getAwaitMoveMessage().getTreasureToFindNext();
			MoveMessageData Move=createMoveMessage(clientBoard.getShiftCard(), clientBoard);
			pinpos.setPinpos(Move.getNewPinPos());
			maze1.setMoveMessage(Move);
			maze1.setMessagetype(MazeComMessagetype.MOVE);
			try {
				outToServer.write(maze1);	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case WIN:
			System.out.println("Der Spieler"+playerId+"hat das Spiel gewonnen!");
			finish = true;
		default:
			break;
		}
	}
	
	public static  PositionData TreasurePositionFinden(BoardData board, Treasure treasure) {
		PositionData treasurepos = new PositionData();
		for (int i = 0; i < board.getRow().size(); i++) {
			for (int j = 0; j < board.getRow().get(i).getCol().size(); j++) {
				if (board.getRow().get(i).getCol().get(j).getTreasure() == treasure) {
					treasurepos.setCol(j);
					treasurepos.setRow(i);
					return treasurepos;
				}
			}
		}
		return treasurepos;
	}

	public static  List<PositionData> ReachablePositionsfinden(BoardData board, int i, int j, boolean[][] Visited_test,List<PositionData> allpositions) {
		if (board.getRow().get(i).getCol().get(j).getOpenings().isBottom()) {
			if (i < 6 && board.getRow().get(i + 1).getCol().get(j).getOpenings().isTop() == true
					&& Visited_test[i + 1][j] == false) {
				Visited_test[i + 1][j] = true;
				PositionData newpos = new PositionData();
				newpos.setRow(i);
				newpos.setCol(j);
				allpositions.add(newpos);
				ReachablePositionsfinden(board, i + 1, j, Visited_test, allpositions);
			}
		}
		// überprüfen,ob es ein Zugang zur linken Karte gibt
		if (board.getRow().get(i).getCol().get(j).getOpenings().isLeft()) {
			if (j > 0 && board.getRow().get(i).getCol().get(j - 1).getOpenings().isRight()
					&& Visited_test[i][j - 1] == false) {
				Visited_test[i][j - 1] = true;
				PositionData newpos = new PositionData();
				newpos.setRow(i);
				newpos.setCol(j);
				allpositions.add(newpos);
				ReachablePositionsfinden(board, i, j - 1, Visited_test, allpositions);
			}
		}
		// überprüfen,ob es ein Zugang zur rechten Karte gibt
		if (board.getRow().get(i).getCol().get(j).getOpenings().isRight()) {
			if (j < 6 && board.getRow().get(i).getCol().get(j + 1).getOpenings().isLeft()
					&& Visited_test[i][j + 1] == false) {
				Visited_test[i][j + 1] = true;
				PositionData newpos = new PositionData();
				newpos.setRow(i);
				newpos.setCol(j);
				allpositions.add(newpos);
				ReachablePositionsfinden(board, i, j + 1, Visited_test, allpositions);
			}
			// überprüfen,ob es ein Zugang zur obenen Karte gibt
					}
			if (board.getRow().get(i).getCol().get(j).getOpenings().isTop()) {
				if (i > 0 && board.getRow().get(i - 1).getCol().get(j).getOpenings().isBottom()
						&& Visited_test[i - 1][j] == false) {
					Visited_test[i - 1][j] = true;
					PositionData newpos = new PositionData();
					newpos.setRow(i);
					newpos.setCol(j);
					allpositions.add(newpos);
					ReachablePositionsfinden(board, i - 1, j, Visited_test, allpositions);
				}
			}
		PositionData newpos = new PositionData();
		newpos.setRow(i);
		newpos.setCol(j);
		allpositions.add(newpos);
		return allpositions;
	}

	public static  BoardData fakeshift(PositionData shiftpos, BoardData board) {
		Board fake = new Board(board);
		CardData newCard, replacementCard;	
		if (shiftpos.getCol() == 0)// horizontal
		{
			newCard = fake.getRow().get(shiftpos.getRow()).getCol().get(6);
			for (int i = 6; i > 0; i--) {
				replacementCard = fake.getRow().get(shiftpos.getRow()).getCol().get(i - 1);
				fake.getRow().get(shiftpos.getRow()).getCol().set(i, replacementCard);
			}
		}
		else if (shiftpos.getCol() == 6)// horizontal schieb
		{
			newCard = fake.getRow().get(shiftpos.getRow()).getCol().get(0);
			for (int i = 0; i < 6; i++) {
				replacementCard = fake.getRow().get(shiftpos.getRow()).getCol().get(i + 1);
				fake.getRow().get(shiftpos.getRow()).getCol().set(i, replacementCard);
			}
		}
		else if (shiftpos.getRow() == 0)// vertical
		{
			newCard = fake.getRow().get(6).getCol().get(shiftpos.getCol());
			for (int i = 6; i >= 1; i--) {		
				replacementCard = fake.getRow().get(i-1).getCol().get(shiftpos.getCol());
				fake.getRow().get(i).getCol().set(shiftpos.getCol(), replacementCard);
			}
		}
		else	// vertical
		{
			newCard = fake.getRow().get(0).getCol().get(shiftpos.getCol());
			for (int i = 0; i < 6; i++) {				
				replacementCard = fake.getRow().get(i + 1).getCol().get(shiftpos.getCol());
				fake.getRow().get(i).getCol().set(shiftpos.getCol(), replacementCard);
			}
		}
		fake.getRow().get(shiftpos.getRow()).getCol().remove(shiftpos.getCol());
		fake.getRow().get(shiftpos.getRow()).getCol().add(shiftpos.getCol(), fake.getShiftCard());
		fake.setShiftCard(newCard);
		return fake;
	}

	public static  PinShift allpossiblesshifts(BoardData board) {
		List<PinShift> AllShiftnPos = new ArrayList<PinShift>();
		BoardData shiftedboard;
		List<PositionData> allReachableCards;
		PinShift besteRowPin = new PinShift();
		boolean found =false;
		allshifts.setAllshifts(allshifts.removeforbiddenPos(verbotenpos));
		listAff(allshifts.getAllshifts());		
		for (int i = 0; i <allshifts.getAllshifts().size(); i++) {
				shiftedboard  = fakeshift(allshifts.getAllshifts().get(i), board);	
				PositionData treasurepos =  TreasurePositionFinden(shiftedboard,t);
				PositionData posdata = findPinInBoard(shiftedboard);
				System.out.println("Position des Pins after shift ROW :"+posdata.getRow()+"COL :"+posdata.getCol());
				allReachableCards = AllReachableCards(shiftedboard,posdata);
				if ( allReachableCards.size() > 1 && PositionDatafound(allReachableCards , treasurepos )==true ) {
					//listAff(allReachableCards);		
					PinShift pinShift = BestePosition(allReachableCards,treasurepos);
					System.out.println("PinShift Row"+ pinShift.getNewpinpos().getRow() +"PinShift Col"+ pinShift.getNewpinpos().getCol());
					pinShift.setShiftpos(allshifts.getAllshifts().get(i));
					AllShiftnPos.add(pinShift);
					found =true;
				}		
				}
		if(found == true ) {
		besteRowPin =LessMoves(AllShiftnPos);
		}else {
			besteRowPin.setShiftpos(RandomShift());	
			shiftedboard = fakeshift(besteRowPin.getShiftpos(),board);
			besteRowPin.setNewpinpos(oneStepMove(findPinInBoard(shiftedboard),shiftedboard));
		}
		System.out.println("Bestposrow :"+besteRowPin.getNewpinpos().getRow() + "Bestposcol :"+besteRowPin.getNewpinpos().getCol());
		return besteRowPin;	
	}
	
	public static  boolean[][] Visited_matrix(PositionData pinpos) {
		boolean[][] Visited_test = new boolean[7][7];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 7; j++) {
				if (i == pinpos.getRow() && j == pinpos.getCol()) {
					Visited_test[i][j] = true;
				}
				Visited_test[i][j] = false;
			}
		}
		return Visited_test;
	}
	
	//get all the reachable cards in an ArrayList
	public static  List<PositionData> AllReachableCards(BoardData board ,PositionData posdata ) {
		boolean Visited_test[][] = Visited_matrix(posdata);
		List<PositionData> allpositions=new ArrayList<PositionData>();
		List<PositionData> allpos = ReachablePositionsfinden(board, posdata.getRow(),
				posdata.getCol(), Visited_test, allpositions);
		return allpos;
	}
		
	//Berwertungsfunktion
	public static  PinShift BestePosition(List<PositionData> allpos , PositionData treasurepos) {
		PinShift bestOne = new PinShift();
		int steps = 0 ;
		for (int i = 1; i < allpos.size(); i++) {
			if (allpos.get(i).getRow() == treasurepos.getRow() && allpos.get(i).getCol() == treasurepos.getCol()){
				PositionData NearestPosition = new PositionData();
				NearestPosition.setRow(allpos.get(i).getRow());
				NearestPosition.setCol(allpos.get(i).getCol());
				steps=i+1;
				bestOne.setNewpinpos(NearestPosition);
				bestOne.setSteps(steps);
				break;
			}
		}
		return bestOne;
	}


	public static  MoveMessageData createMoveMessage(CardData shiftcard, BoardData ServerBoard) {
		MoveMessageData move = new MoveMessageData();
		PinShift allpins = allpossiblesshifts(ServerBoard);
		move.setNewPinPos(allpins.getNewpinpos());
		move.setShiftCard(shiftcard);
		move.setShiftPosition(allpins.getShiftpos());
		verbotenpos = verbotenePos(move.getShiftPosition());
		System.out.println("forbiddenpos Row :"+verbotenpos.getRow()+"forbiddenpos Col :"+verbotenpos.getCol());
		return move;
	}
	
	
	public static void listAff(List<PositionData> allpos){
		System.out.print("{");
		for(int i=0;i<allpos.size();i++){
			System.out.println("[ Row :"+allpos.get(i).getRow()+", Col :"+allpos.get(i).getCol()+"],");
		}
		System.out.println("}");
	}
	
	public static PinShift LessMoves(List<PinShift> allpos ){
		PinShift nearestPosition=allpos.get(0);
		for (int i = 1; i < allpos.size(); i++) {
			if (allpos.get(i).getSteps() < nearestPosition.getSteps()) {
				nearestPosition.setNewpinpos(allpos.get(i).getNewpinpos());
				nearestPosition.setShiftpos(allpos.get(i).getShiftpos());
			}
		}
		return nearestPosition;	
	}
	
	//Remove the unwanted shifts
	public static  PositionData verbotenePos(PositionData shiftPos){
		PositionData verboteneShiftpos = new PositionData();
		if (shiftPos.getCol() == 0) { // horizontal
			verboteneShiftpos.setCol(shiftPos.getCol()+6);
			verboteneShiftpos.setRow(shiftPos.getRow());
			
		}else if(shiftPos.getRow() == 0) {
			verboteneShiftpos.setCol(shiftPos.getCol());
			verboteneShiftpos.setRow(shiftPos.getRow()+6);
			
		}else if(shiftPos.getRow()== 6) {
			verboteneShiftpos.setCol(shiftPos.getCol());
			verboteneShiftpos.setRow(shiftPos.getRow()-6);
		}else {
			verboteneShiftpos.setCol(shiftPos.getCol()-6);
			verboteneShiftpos.setRow(shiftPos.getRow());
		}
		return verboteneShiftpos;
	}
		
	public static  PositionData findPinInBoard(BoardData board) {
		PositionData posd = new PositionData();
		int k = 0;
		for(int i = 0 ; i < board.getRow().size();i++) {
			for(int j = 0 ;j<board.getRow().get(0).getCol().size();j++) {
				if(board.getRow().get(i).getCol().get(j).getPin().getPlayerID().size() > 0){
					//System.out.println("la position du pin Row :"+i+"Col :"+j);
					for(int o = 0 ;o<board.getRow().get(i).getCol().get(j).getPin().getPlayerID().size();o++) {
					if(board.getRow().get(i).getCol().get(j).getPin().getPlayerID().get(o)==playerId){
					System.out.println(playerId);
					posd.setRow(i);
					posd.setCol(j);
					k=1;
					break;
					}
					}
				}			
			}
			if(k==1){
				break;
			}
		}
    	System.out.println("Row :"+posd.getRow() +"Col :"+posd.getCol());
		return posd;
	}
	
    public static  PositionData oneStepMove(PositionData PositionD ,BoardData board){
    	//System.out.println("Row :"+PositionD.getRow() +"Col :"+PositionD.getCol());
    	//PositionData Pos = new PositionData();
    	if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isRight()){
    		if(PositionD.getCol()< 6 &&board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()+1).getOpenings().isLeft()) {
    			PositionD.setCol(PositionD.getCol()+1);
    		}
    	}
    	else{
    			if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isBottom()){
    				if(PositionD.getRow()<6 && board.getRow().get(PositionD.getRow()+1).getCol().get(PositionD.getCol()).getOpenings().isTop()) {
        			PositionD.setRow(PositionD.getRow()+1);
    				}
    			}
    			else{
    			if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isLeft()){
    				if(PositionD.getCol()> 0 &&board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()-1).getOpenings().isRight()) {
        			PositionD.setCol(PositionD.getCol()-1);
    				}
    			else{
    				if(board.getRow().get(PositionD.getRow()).getCol().get(PositionD.getCol()).getOpenings().isTop()){
    					if(PositionD.getRow()> 0 &&board.getRow().get(PositionD.getRow()-1).getCol().get(PositionD.getCol()).getOpenings().isBottom()) {
    	    			PositionD.setRow(PositionD.getRow()-1);
    					}
    				}
    			}
	
    			}
    			}
    	}
    	
    	return PositionD;
    }
    
    public static  boolean PositionDatafound(List<PositionData> allReachablepos , PositionData treasurepos){
    	boolean found = false;
    	for (int i = 0; i < allReachablepos.size(); i++) {
			if (allReachablepos.get(i).getRow() == treasurepos.getRow() && allReachablepos.get(i).getCol() == treasurepos.getCol()){
				found = true;
				break;
			}		
    }
    	return found;
    }	
    
    public static  PositionData  RandomShift(){
    return allshifts.getAllshifts().get(randomNumber());
    }
    
    public static int randomNumber() {
	   return (int) (Math.random()*11);
   }
}

