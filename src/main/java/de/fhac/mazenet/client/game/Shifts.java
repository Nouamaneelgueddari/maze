package de.fhac.mazenet.client.game;

import java.util.ArrayList;
import java.util.List;

import de.fhac.mazenet.server.generated.PositionData;

public class Shifts {
	private  List<PositionData>allshifts = new ArrayList<PositionData>();
	public Shifts(){ 
		AddPosition();
			
	}
	
	public List<PositionData> getAllshifts() {
		return allshifts;
	}

	public void setAllshifts(List<PositionData> allshifts) {
		this.allshifts = allshifts;
	}

	private void AddPosition() {
		allshifts.add(new ShiftPositionData(0,1).getShiftpos());
		allshifts.add(new ShiftPositionData(0,3).getShiftpos());
		allshifts.add(new ShiftPositionData(0,5).getShiftpos());
		allshifts.add(new ShiftPositionData(1,0).getShiftpos());
		allshifts.add(new ShiftPositionData(3,0).getShiftpos());
		allshifts.add(new ShiftPositionData(5,0).getShiftpos());
		allshifts.add(new ShiftPositionData(6,1).getShiftpos());
		allshifts.add(new ShiftPositionData(6,3).getShiftpos());
		allshifts.add(new ShiftPositionData(6,5).getShiftpos());
		allshifts.add(new ShiftPositionData(1,6).getShiftpos());
		allshifts.add(new ShiftPositionData(3,6).getShiftpos());
		allshifts.add(new ShiftPositionData(5,6).getShiftpos());
	}

	public List<PositionData> removeforbiddenPos(PositionData removethis) 
	{	List<PositionData> newShifts= new ArrayList<PositionData>();
		for(int i = 0;i<allshifts.size();i++) {
			if(allshifts.get(i).getRow() == removethis.getRow() && allshifts.get(i).getCol() == removethis.getCol()) {
				continue;
			}
			newShifts.add(allshifts.get(i));
		}
		return newShifts;
	}

	
}
