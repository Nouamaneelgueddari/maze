package de.fhac.mazenet.client.game;

import de.fhac.mazenet.server.generated.PositionData;

public class PinPosition {
	PositionData pinpos = new PositionData();
	
	public PinPosition(int Row,int Col){
		pinpos.setCol(Col);
		pinpos.setRow(Row);
	}

	public PositionData getPinpos() {
		return pinpos;
	}

	public void setPinpos(PositionData pinpos) {
		this.pinpos = pinpos;
	}

}
