package de.fhac.mazenet.client.game;

import de.fhac.mazenet.server.generated.PositionData;

public class PinShift {
	PositionData newpinpos;
	PositionData shiftpos;
	int Steps;
	
	public int getSteps() {
		return Steps;
	}


	public void setSteps(int steps) {
		Steps = steps;
	}


	public PinShift(PositionData newpinpos , PositionData shiftpos) 
	{
	 this.newpinpos = newpinpos;
	 this.shiftpos=shiftpos;
	}
	
	
	public PinShift() {
		super();
		// TODO Auto-generated constructor stub
	}


	@Override
	public String toString() {
		return "PinShift [newRowpinpos=" + newpinpos.getRow() + ", newColpinpos=" + newpinpos.getCol() +
				" "+"SihftRowposition= "+shiftpos.getRow() +"SihftColposition="+shiftpos.getCol()+"]";
	}


	public PositionData getNewpinpos() {
		return newpinpos;
	}
	public void setNewpinpos(PositionData newpinpos) {
		this.newpinpos = newpinpos;
	}
	public PositionData getShiftpos() {
		return shiftpos;
	}
	public void setShiftpos(PositionData shiftpos) {
		this.shiftpos = shiftpos;
	}

}
