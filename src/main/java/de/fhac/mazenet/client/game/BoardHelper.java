package de.fhac.mazenet.client.game;

import de.fhac.mazenet.server.generated.BoardData;

public class BoardHelper{
	
	public static int hashCode(BoardData boarddata) {
		StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < boarddata.getRow().size(); ++i) {
            for (int j = 0; j < boarddata.getRow().get(i).getCol().size(); ++j) {
            	//Openings opening = boarddata.getRow().get(i).getCol().get(j).getOpenings();
            	var treasure = boarddata.getRow().get(i).getCol().get(j).getTreasure();
            	var pin = boarddata.getRow().get(i).getCol().get(j).getPin();
            	/*stringBuilder.append(opening.isTop());
            	stringBuilder.append(opening.isRight());
            	stringBuilder.append(opening.isBottom());
            	stringBuilder.append(opening.isLeft());*/
            	
            	if(treasure != null)
            		stringBuilder.append(treasure.value());
            	
            	for(int playerId : pin.getPlayerID())
            		stringBuilder.append(playerId);
            }
        }
		return stringBuilder.toString().hashCode();
	}
}
