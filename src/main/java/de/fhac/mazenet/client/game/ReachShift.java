package de.fhac.mazenet.client.game;

import java.util.List;

import de.fhac.mazenet.server.generated.PositionData;

public class ReachShift {
	PositionData shiftpos;
	List<PositionData>AllReachablepos;
	
	
	public ReachShift() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReachShift(PositionData shiftpos, List<PositionData> allReachablepos) {
		super();
		this.shiftpos = shiftpos;
		AllReachablepos = allReachablepos;
	}
	
	public PositionData getShiftpos() {
		return shiftpos;
	}

	public void setShiftpos(PositionData shiftpos) {
		this.shiftpos = shiftpos;
	}

	public List<PositionData> getAllReachablepos() {
		return AllReachablepos;
	}

	public void setAllReachablepos(List<PositionData> allReachablepos) {
		AllReachablepos = allReachablepos;
	}


	

}
