package de.fhac.mazenet.client.game;

import de.fhac.mazenet.server.generated.PositionData;

public class ShiftPositionData {
	private PositionData shiftpos=new PositionData();

	public ShiftPositionData(int Row,int col){
		shiftpos.setRow(Row);
		shiftpos.setCol(col);
	}

	public PositionData getShiftpos() {
		return shiftpos;
	}

	public void setShiftpos(PositionData shiftpos) {
		this.shiftpos = shiftpos;
	}
	
	
	
}
